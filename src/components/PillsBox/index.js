import React from 'react'
import Pill from '../Pill'
import MobileNav from '../MobileNav'

const places = [
  'Alaska',
  'Arabian Gulf',
  'Asia',
  'Australia & New Zealand',
  'Bahamas',
  'Bermuda',
  'Canada & New England',
  'Caribbean',
  'Cuba',
  'Europe',
  'Hawaii',
  'Panama Canal',
  'Repositioning',
  'South Pacific',
  'Trasatlantic',
  'Transpacific'
];

const PillsBox = ({ closeModal }) =>
  <div className="pillContent">
    <MobileNav closeModal={closeModal} title="Select Destination" />
    {places.map(pill => <Pill name={pill} key={pill} />)}
  </div>

export default PillsBox