import React from 'react'
import MobileNav from '../MobileNav'

const locations = [
  'Baltimore, Maryland',
  'Boston, Massachusetts',
  'Cape Liberty, New Jersey',
  'Fort Lauderdale, Florida',
  'Galveston, Texas',
  'Honolulu (Oahu), Hawaii',
  'Los Angeles, California',
  'Miami, Florida',
  'New Orleans, Lousiana',
  'Orlando (Port Canaveral), FL',
  'Quebec City, Quebec, Canada',
  'San Diego, California',
  'San Juan, Puerto Rico',
  'Seattle, Washington',
  'Seward, Alaska',
  'Tampa, Florida',
  'Vancouver, British Columbia'
];

const Departing = ({ closeModal }) =>
  <div className="departing">
    <MobileNav closeModal={closeModal} title="Select Departure Port" />
    <div className="departing__header">
      <p className="departing__headerText active">THE AMERICAS & THE CARIBBEAN</p>
      <p className="departing__headerText">EUROPE</p>
      <p className="departing__headerText">ASIA & SOUTHPACIFIC</p>
    </div>
    <div className="departing__content">
      {locations.map(location =>
        <p className="departing__contentText" key={location}>{location}</p>)}
    </div>
  </div>

export default Departing