import React, { useState } from 'react'

const Pill = ({ name }) => {
  const [selected, setSelected] = useState(false)
  return (
    <div className={`pill ${selected ? 'selected' : ''}`} onClick={() => setSelected(!selected)}>
      {name}
    </div>)
}

export default Pill