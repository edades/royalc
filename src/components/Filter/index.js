import React from 'react';

const Filter = ({ filter, onSelect }) => {

  const filterRef = React.createRef();

  const clickHandler = () => {
    if (window.innerWidth > 749) {
      const isOpen = filterRef.current.classList.contains('selected');
      const filtersElements = Array.from(document.querySelectorAll('.cruise-search-widget__filter'));
      filtersElements.forEach(filterItem => filterItem.classList.remove('selected'))
      if (isOpen) {
        filterRef.current.classList.remove('selected')
        onSelect('none');
      } else {
        filterRef.current.classList.add('selected')
        onSelect(filter);
      }
    } else {
      onSelect(filter);
    }
  }

  return (
    <div
      className="cruise-search-widget__filter"
      ref={filterRef}
      onClick={clickHandler}>
      <div className="cruise-search-widget__label">{filter.label}</div>
      <div className="cruise-search-widget__selector">
        <span className="cruise-search-widget__selection">{filter.selection}</span>
        <span className="cruise-search-widget__icon cruise-search-widget__icon__caret">
          <i class="material-icons">
            {filter.icon}
          </i>
        </span>
      </div>
    </div>)
}

export default Filter