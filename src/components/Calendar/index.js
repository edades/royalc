import React from 'react';

const months = [
  'Jan',
  'Feb',
  'Mar',
  'Apr',
  'May',
  'Jun',
  'Jul',
  'Aug',
  'Sep',
  'Oct',
  'Nov',
  'Dec'
];

const Calendar = ({ year }) =>
  <div className="calendar">
    <h2 className="calendar__title">{year}</h2>
    <div className="calendar__content">
      {months.map(month => {
        let selectedDate = 'calendar__month'
        
        if (year === '2018') {
          if (month === 'Jul') selectedDate += ' start'
          if (month ==='Aug') selectedDate += ' selected'
          if (month ==='Sep') selectedDate += ' selected'
          if (month === 'Oct') selectedDate += ' end'
        }

        return <div className={selectedDate} key={month}><span>{month}</span></div>
      })}
    </div>
  </div>

export default Calendar