import React from 'react'

const MobileNav = ({ closeModal, title }) =>
  <div className="mobileNav" onClick={closeModal}>
    <i className="mobileNav__icon material-icons">keyboard_arrow_left</i>
    <p className="mobileNav__text">{title}</p>
    <p className="mobileNav__link">APPLY</p>
  </div>

export default MobileNav