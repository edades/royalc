import React from 'react'
import Button from '../Button'
import Filter from '../Filter'

const filtersData = [
  {
    id: 'to',
    label: 'Cruising to',
    selection: 'Bermuda',
    icon: 'keyboard_arrow_down'
  },
  {
    id: 'from',
    label: 'Departing from',
    selection: 'Any Departure Port',
    icon: 'keyboard_arrow_down'
  },
  {
    id: 'date',
    label: 'Leaving',
    selection: "Apr '20 - Jun '20",
    icon: 'calendar_today'
  },
]

const Toolbar = ({ setFilterContent }) => {
  const selectFilter = filter => setFilterContent(filter.id)

  return (
    <div className="cruise-search-widget__toolbar">
      <h1 className="cruise-search-widget__toolbar__title">Find a Cruise</h1>
      {filtersData.map(filter =>
        <Filter filter={filter} key={filter.id} onSelect={selectFilter}/>)}
      <div className="cruise-search-widget__cta cruise-search-widget__cta--align-right">
        <Button />
      </div>
    </div>)
}

  export default Toolbar