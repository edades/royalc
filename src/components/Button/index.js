import React from 'react';

const Button = () =>
  <button type="button" className="md-button md-raised md-primary md-elevation-0 md-button-no-margin cruise-search-widget__button md-theme-rccl" id="rciCruiseSearchWidgetSubmitButton">
    <div className="md-ink-ripple">
      <div className="md-ripple"></div>
    </div>
    <span>{window.innerWidth > 749 ? 'Search Cruises' : 'Find a cruise'}</span>
  </button>

export default Button;