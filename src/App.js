import React, { useState } from 'react';
import Toolbar from './components/Toolbar'
import PillsBox from './components/PillsBox'
import Departing from './components/Departing'
import Calendar from './components/Calendar'
import MobileNav from './components/MobileNav'

const App = () => {
  const [selectedFilter, setSelectedFilter] = useState('')

  const setFilterContent = id => setSelectedFilter(id);

  const showContent = () => {
    switch (selectedFilter) {
      case 'to':
        return <PillsBox closeModal={() => setFilterContent('')}/>
      case 'from':
        return <Departing closeModal={() => setFilterContent('')}/>
      case 'date':
        return (
          <div className="calendar__box">
            <MobileNav closeModal={() => setFilterContent('')} title="Select Travel Dates" />
            <Calendar year="2018"/>
            <Calendar year="2019"/>
            <Calendar year="2020"/>
          </div>
        )
      default:
        return ''
    }
  }

  return (
    <div className="App">
      <Toolbar setFilterContent={setFilterContent}/>
      <div className="App__container">
        {showContent()}
      </div>
    </div>
  );
}

export default App;
